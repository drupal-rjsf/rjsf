<?php

namespace Drupal\rjsf\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Rjsf filter plugins.
 */
abstract class FilterPluginBase extends PluginBase implements FilterPluginInterface {

  /**
   * The JSON Schema type that the plugin is valid for.
   *
   * @var array
   */
  protected array $type;

  /**
   * {@inheritdoc}
   */
  public function getType(): array {
    return $this->type;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($value, array $args = []): bool {
    return TRUE;
  }

}
