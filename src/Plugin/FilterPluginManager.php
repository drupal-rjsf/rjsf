<?php

namespace Drupal\rjsf\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides the Rjsf plugin manager.
 */
class FilterPluginManager extends DefaultPluginManager {

  /**
   * Constructs a new FilterPluginManager object.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Rjsf/Filter',
      $namespaces,
      $module_handler,
      'Drupal\rjsf\Plugin\FilterPluginInterface',
      'Drupal\rjsf\Annotation\RjsfFilter'
    );

    $this->alterInfo('rjsf_filter_info');
    $this->setCacheBackend($cache_backend, 'rjsf_filter_plugins');
  }

}
