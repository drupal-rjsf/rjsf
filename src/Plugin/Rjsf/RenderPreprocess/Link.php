<?php

namespace Drupal\rjsf\Plugin\Rjsf\RenderPreprocess;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\rjsf\Plugin\RenderPreprocessPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Load url for a link field.
 *
 * @RjsfRenderPreprocess(
 *  id = "link",
 *  label = @Translation("Link"),
 * )
 */
class Link extends RenderPreprocessPluginBase implements ContainerFactoryPluginInterface {

  use LoggerChannelTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Link constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $this->getLogger('rjsf');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * Get urls for a link field.
   */
  public function preprocess($value, array $vars = [], array $schema = [], array $uiSchema = []) {
    $processed = $value;

    if (empty($value['url']) && empty($value['entity'])) {
      $processed['url'] = null;
    }
    elseif ($value['url']) {
      $url = trim($value['url']);
      if (in_array($url, ['<nolink>', '<none>', '<button>', '<front>'])) {
        $processed['url'] = Url::fromRoute($url)->toString();
      }
      elseif (in_array($url[0], ['#', '?', '/'])) {
        $processed['url'] = Url::fromUserInput($url)->toString();
      }
      else {
        $processed['url'] = Url::fromUri($url)->toString();
      }
    }
    elseif ($value['entity']) {
      if (!isset($value['entity']['type'])) {
        $this->logger->warning('Link to preprocessor tried to load an entity with @uuid without declaring an entity type', ['@uuid' => $value['entity']['uuid']]);
      }

      $entity = $this->entityTypeManager->getStorage($value['entity']['type'])->loadByProperties(
        ['uuid' => $value['entity']['uuid']]
      );
      $entity = reset($entity);
      $processed['url'] = $entity->toUrl()->toString();
      $processed['entity'] = $entity;

      $cache = new CacheableMetadata();
      $cache->setCacheContexts($entity->getCacheContexts());
      $cache->setCacheMaxAge($entity->getCacheMaxAge());
      $cache->setCacheTags($entity->getCacheTags());

      $this->mergeCacheableMetadata($cache);
    }

    return $processed;
  }

}
