<?php

namespace Drupal\rjsf\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides the Rjsf Render Preprocess manager.
 */
class RenderPreprocessPluginManager extends DefaultPluginManager {

  /**
   * Constructs a new RjsfRenderPreprocessManager object.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Rjsf/RenderPreprocess',
      $namespaces,
      $module_handler,
      'Drupal\rjsf\Plugin\RenderPreprocessPluginInterface',
      'Drupal\rjsf\Annotation\RjsfRenderPreprocess'
    );

    $this->alterInfo('rjsf_render_preprocess_info');
    $this->setCacheBackend($cache_backend, 'rjsf_render_preprocess_plugins');
  }

}
