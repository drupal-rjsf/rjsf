<?php

namespace Drupal\rjsf\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'rjsf_editor' field type.
 *
 * @FieldType(
 *   id = "rjsf_data",
 *   label = @Translation("RJSF Data"),
 *   description = @Translation("Collect data using an RJSF form."),
 *   default_widget = "rjsf_editor_widget",
 *   default_formatter = "rjsf_editor_formatter",
 *   list_class = "\Drupal\rjsf\Plugin\Field\FieldType\RjsfDataFieldItemList",
 * )
 */
class RjsfDataItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'json',
          'pgsql_type' => 'json',
          'mysql_type' => 'json',
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
        'server_validation' => TRUE,
        'client_validation' => FALSE,
      ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('JSON value'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];
    $settings = $this->getSettings();

    $element['server_validation'] = [
      '#type' => 'checkbox',
      '#title' => t('Serverside validation'),
      '#default_value' => $settings['server_validation'],
      '#description' => t('When enabled inputs will be validated serverside.'),
    ];

    $element['client_validation'] = [
      '#type' => 'checkbox',
      '#title' => t('Live validation'),
      '#default_value' => $settings['client_validation'],
      '#description' => t('When enabled inputs will be validated on the browser side by ReactJSONSchemaForm.'),
    ];

    return $element;
  }


  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
        'schema' => [],
        'uiSchema' => [],
        'renderPreprocess' => [],
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $elements = [];

    $elements['schema'] = [
      '#type' => 'textarea',
      '#title' => t('RJSF Schema'),
      '#default_value' => $this->getSetting('schema'),
      '#required' => TRUE,
      '#element_validate' => [[static::class, 'isJsonValue']],
    ];
    $elements['uiSchema'] = [
      '#type' => 'textarea',
      '#title' => t('RJSF Schema UI'),
      '#default_value' => $this->getSetting('uiSchema'),
      '#required' => TRUE,
      '#element_validate' => [[static::class, 'isJsonValue']],
    ];
    $elements['renderPreprocess'] = [
      '#type' => 'textarea',
      '#title' => t('RJSF Render Preprocess'),
      '#default_value' => $this->getSetting('renderPreprocess'),
      '#required' => FALSE,
      '#element_validate' => [[static::class, 'isJsonValue']],
    ];

    return $elements;
  }

  public static function isJsonValue($element, FormStateInterface $form_state) {
    $value = $element['#value'];

    if (json_decode($value) === NULL) {
      $form_state->setError($element, t('Value must be valid JSON.'));
    }
  }

}
