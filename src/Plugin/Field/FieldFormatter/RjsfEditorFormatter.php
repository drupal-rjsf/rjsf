<?php

namespace Drupal\rjsf\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'rjsf_editor_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "rjsf_editor_formatter",
 *   label = @Translation("RJSF Editor Formatter"),
 *   field_types = {
 *     "rjsf_editor"
 *   }
 * )
 */
class RjsfEditorFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#markup' => $item->value,
      ];
    }

    return $elements;
  }

}
