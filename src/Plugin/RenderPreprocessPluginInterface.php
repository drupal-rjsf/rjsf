<?php

namespace Drupal\rjsf\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Cache\CacheableMetadata;

/**
 * Defines an interface for Rjsf plugins.
 */
interface RenderPreprocessPluginInterface extends PluginInspectionInterface {

  /**
   * Get the cacheable metadata resulting from this preprocess.
   *
   * @return \Drupal\Core\Cache\CacheableMetadata
   */
  public function getCacheableMetadata(): CacheableMetadata;

  /**
   * Set the cacheable metadata resulting from this preprocess.
   *
   * @param \Drupal\Core\Cache\CacheableMetadata $cacheableMetadata
   */
  public function setCacheableMetadata(CacheableMetadata $cacheableMetadata): void;

  /**
   * Merge cacheable metadata into the existing cacheable metadata.
   *
   * @param \Drupal\Core\Cache\CacheableMetadata $cacheableMetadata
   */
  public function mergeCacheableMetadata(CacheableMetadata $cacheableMetadata): void;

  /**
   * Preprocess a data value.
   *
   * @param $value
   *   The value for the field being preprocessed.
   * @param array $vars
   *   The vars set in the preprocessor config.
   * @param array $schema
   *   The schema for the field being preprocessed.
   * @param array $uiSchema
   *   The uiSchema for the field being preprocessed.
   *
   * @return mixed
   */
  public function preprocess($value, array $vars = [], array $schema = [], array $uiSchema = []);

}
