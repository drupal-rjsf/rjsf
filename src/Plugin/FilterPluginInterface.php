<?php

namespace Drupal\rjsf\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Rjsf filter plugins.
 */
interface FilterPluginInterface extends PluginInspectionInterface {

  /**
   * Gets the type the plugin is valid for.
   *
   * @return array
   *   The format of the plugin instance.
   */
  public function getType(): array;

  /**
   * Determine if a data value is valid for the given filter.
   *
   * @param $value
   *   The value to validate.
   * @param array $args
   *   (optional) The args configured for the filter.
   *
   * @return bool
   *   TRUE if the value passes validation, FALSE otherwise.
   */
  public function validate($value, array $args = []): bool;

}
