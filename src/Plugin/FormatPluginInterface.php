<?php

namespace Drupal\rjsf\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Rjsf format plugins.
 */
interface FormatPluginInterface extends PluginInspectionInterface {

  /**
   * Get the JSON schema type the plugin is valid for.
   *
   * @return array
   *   The format of the plugin instance.
   */
  public function getType(): array;

  /**
   * Determine if a data value is valid for the given format.
   *
   * @param $value
   *   The value to validate.
   *
   * @return bool
   *   TRUE if the value passes validation, FALSE otherwise.
   */
  public function validate($value): bool;

}
