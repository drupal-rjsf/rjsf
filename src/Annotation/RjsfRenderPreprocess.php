<?php

namespace Drupal\rjsf\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines a RjsfRenderPreprocess item annotation object.
 *
 * @see \Drupal\rjsf\Plugin\RjsfManager
 * @see plugin_api
 *
 * @Annotation
 */
class RjsfRenderPreprocess extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public string $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $label;

  /**
   * The JSON Schema type that the plugin applies to.
   *
   * @var array
   */
  public array $type;

  /**
   * The JSON Schema format that the plugin applies to.
   *
   * @var array
   */
  public array $format = [];

  /**
   * The JSON Schema filter that the plugin applies to.
   *
   * @var array
   */
  public $filter = [];

}
