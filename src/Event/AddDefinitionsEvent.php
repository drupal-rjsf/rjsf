<?php

namespace Drupal\rjsf\Event;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Render\BubbleableMetadata;

/**
 * Defines the event for adding additional definitions to the rjsf schema.
 *
 * @see \Drupal\rjsf\Event\RjsfEvents
 */
class AddDefinitionsEvent extends Event {

  /**
   * The RJSF schema being used with the editor.
   *
   * @var array
   */
  protected array $schema;

  /**
   * The RJSF uiSchema being used with the editor.
   *
   * @var array
   */
  protected array $ui_schema;

  /**
   * The form the editor is being attached to.
   *
   * @var array
   */
  protected array $form;

  /**
   * The RJSF form element.
   *
   * @var array
   */
  protected array $element;

  /**
   * The form state of the form the editor is being attached to.
   *
   * @var \Drupal\Core\Form\FormStateInterface
   */
  protected FormStateInterface $form_state;

  /**
   * The definitions to add to the schema.
   *
   * @var array
   */
  protected array $definitions = [];

  /**
   * Constructs a new AddDefinitionsEvent object.
   *
   * @param array $schema
   *    The RJSF schema being used with the editor.
   * @param array $ui_schema
   *    The RJSF uiSchema being used with the editor.
   * @param array $element
   *   The RJSF form element.
   * @param array $form
   *    The form the editor is being attached to.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *    The form state of the form the editor is being attached to.
   */
  public function __construct(array $schema, array $ui_schema, array $element, array $form, FormStateInterface $form_state) {
    $this->schema = $schema;
    $this->ui_schema = $ui_schema;
    $this->element = $element;
    $this->form = $form;
    $this->form_state = $form_state;
  }

  /**
   * Get the RJSF schema.
   *
   * @return array
   */
  public function getSchema(): array {
    return $this->schema;
  }

  /**
   * Get the RJSF uiSchema.
   *
   * @return array
   */
  public function getUiSchema(): array {
    return $this->ui_schema;
  }

  /**
   * @return array
   */
  public function getElement(): array {
    return $this->element;
  }

  /**
   * Get the form.
   *
   * @return array
   */
  public function getForm(): array {
    return $this->form;
  }

  /**
   * Get the form state.
   *
   * @return \Drupal\Core\Form\FormStateInterface
   */
  public function getFormState() {
    return $this->form_state;
  }

  /**
   * Add definitions.
   *
   * @param array $definitions
   */
  public function addDefinitions(array $definitions) {
    $this->definitions = array_merge_recursive($this->definitions, $definitions);
  }

  /**
   * The definitions to be added to the schema.
   *
   * @return array
   */
  public function getDefinitions(): array {
    return $this->definitions;
  }

}
