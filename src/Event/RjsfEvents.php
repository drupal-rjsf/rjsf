<?php

namespace Drupal\rjsf\Event;

/**
 * Defines events for the base RJSF module.
 */
final class RjsfEvents {

  /**
   * Name of the event fired when attaching the editor to a form.
   *
   * @Event
   *
   * @see \Drupal\rjsf\Event\EditorAttachEvent
   */
  const EDITOR_ATTACH = 'rjsf.editor_attach';

  /**
   * Name of the event fired when adding definitions to a schema form.
   *
   * @Event
   *
   * @see \Drupal\rjsf\Event\AddDefinitionsEvent
   */
  const ADD_DEFINITIONS = 'rjsf.add_definitions';

}
