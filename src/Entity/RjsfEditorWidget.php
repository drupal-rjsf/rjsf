<?php

namespace Drupal\rjsf\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the RJSF Editor Widget entity.
 *
 * @ConfigEntityType(
 *   id = "rjsf_editor_widget",
 *   label = @Translation("RJSF Editor Widget"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\rjsf\RjsfEditorWidgetListBuilder",
 *     "form" = {
 *       "add" = "Drupal\rjsf\Form\RjsfEditorWidgetForm",
 *       "edit" = "Drupal\rjsf\Form\RjsfEditorWidgetForm",
 *       "delete" = "Drupal\rjsf\Form\RjsfEditorWidgetDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\rjsf\RjsfEditorWidgetHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\rjsf\RjsfEditorWidgetAccessControlHandler",
 *   },
 *   config_prefix = "rjsf_editor_widget",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/rjsf/rjsf_editor_widget/{rjsf_editor_widget}",
 *     "add-form" = "/admin/structure/rjsf/rjsf_editor_widget/add",
 *     "edit-form" = "/admin/structure/rjsf/rjsf_editor_widget/{rjsf_editor_widget}/edit",
 *     "delete-form" = "/admin/structure/rjsf/rjsf_editor_widget/{rjsf_editor_widget}/delete",
 *     "collection" = "/admin/structure/rjsf/rjsf_editor_widget"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "enable",
 *     "url",
 *     "jsPlugin",
 *     "jsScope"
 *   },
 * )
 */
class RjsfEditorWidget extends ConfigEntityBase implements RjsfEditorWidgetInterface {

  // @TODO figure out permissions so that users don't need "administer site config" to read the values through json api.

  /**
   * The Editor widgets ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Editor widgets label.
   *
   * @var string
   */
  protected $label;

  /**
   * The enabled state of the widget.
   *
   * @var boolean
   */
  protected $enable = FALSE;

  /**
   * The url to the widget entry point.
   *
   * @var string
   */
  protected $url = '';

  /**
   * The javascript plugin name.
   *
   * @var string
   */
  protected $jsPlugin = '';

  /**
   * The javascript module scope.
   *
   * @var string
   */
  protected $jsScope = '';

  /**
   * @return bool
   */
  public function isEnable(): bool {
    return $this->enable;
  }

  /**
   * @param bool $enable
   */
  public function setEnable(bool $enable): void {
    $this->enable = $enable;
  }

  /**
   * @return string
   */
  public function getUrl(): string {
    return $this->url;
  }

  /**
   * @param string $url
   */
  public function setUrl(string $url): void {
    $this->url = $url;
  }

  /**
   * @return string
   */
  public function getJsPlugin(): string {
    return $this->jsPlugin;
  }

  /**
   * @param string $jsPlugin
   */
  public function setJsPlugin(string $jsPlugin): void {
    $this->jsPlugin = $jsPlugin;
  }

  /**
   * @return string
   */
  public function getJsScope(): string {
    return $this->jsScope;
  }

  /**
   * @param string $jsScope
   */
  public function setJsScope(string $jsScope): void {
    $this->jsScope = $jsScope;
  }

}
