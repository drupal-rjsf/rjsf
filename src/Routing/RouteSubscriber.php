<?php

namespace Drupal\rjsf\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber.
 *
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('jsonrpc.handler')) {
      $auth = $route->getOption('_auth');
      $auth[] = 'cookie';
      $route->setOption('_auth', $auth);
    }
  }
}
