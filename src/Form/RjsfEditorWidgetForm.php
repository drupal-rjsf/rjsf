<?php

namespace Drupal\rjsf\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class RjsfEditorWidgetForm.
 */
class RjsfEditorWidgetForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $rjsf_editor_widget = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $rjsf_editor_widget->label(),
      '#description' => $this->t("Label for the RJSF Editor Widget."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $rjsf_editor_widget->id(),
      '#machine_name' => [
        'exists' => '\Drupal\rjsf\Entity\RjsfEditorWidget::load',
      ],
      '#disabled' => !$rjsf_editor_widget->isNew(),
    ];

    $form['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Url'),
      '#default_value' => $rjsf_editor_widget->getUrl(),
      '#description' => $this->t("Label for the Editor widgets."),
      '#required' => TRUE,
    ];

    $form['jsPlugin'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Widget plugin'),
      '#default_value' => $rjsf_editor_widget->getJsPlugin(),
      '#description' => $this->t("The name of the plugin as defined in webpack"),
      '#required' => TRUE,
    ];

    $form['jsScope'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Widget scope'),
      '#default_value' => $rjsf_editor_widget->getJsScope(),
      '#description' => $this->t("The scope of the module as defined in webpack"),
      '#required' => TRUE,
    ];

    $form['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
      '#default_value' => $rjsf_editor_widget->isEnable(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $rjsf_editor_widget = $this->entity;
    $status = $rjsf_editor_widget->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label RJSF Editor Widget.', [
          '%label' => $rjsf_editor_widget->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label RJSF Editor Widget.', [
          '%label' => $rjsf_editor_widget->label(),
        ]));
    }
    $form_state->setRedirectUrl($rjsf_editor_widget->toUrl('collection'));
  }

}
