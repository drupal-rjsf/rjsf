<?php

namespace Drupal\rjsf\Element;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\Element\FormElement;
use Drupal\rjsf\Event\AddDefinitionsEvent;
use Drupal\rjsf\Event\EditorAttachEvent;
use Drupal\rjsf\Event\RjsfEvents;

/**
 * Provides a form element to integrate an RJSF Editor.
 *
 * Required options are:
 * - #schema (array)
 *   The schema definition represented as a PHP array for RJSF to render.
 *
 * Optional options are:
 * - #uiSchema (array)
 *   (optional) The uiSchema definition represented as a PHP array for RJSF to
 *              render.
 * - #server_validation (bool)
 *   (optional) If the inputs should be validated using PHP. Defaults to TRUE.
 *
 * - #client_validation (bool)
 *   (optional) If the inputs should be validated browser side by RJSF. Defaults to TRUE.
 *
 * Usage example:
 * @code
 * $form['rjsf_element'] = [
 *   '#type' => 'rjsf_editor',
 *   '#schema' => [
 *     'title' => 'A registration form',
 *     'description' => 'A simple form example.',
 *     'type' => 'object',
 *     'required' => ['firstName', 'lastName'],
 *     'properties' => [
 *       'firstName' => [
 *         'type' => 'string',
 *         'title' => 'First name',
 *         'default' => 'Chuck',
 *       ],
 *       'lastName' => [
 *         'type' => 'string',
 *         'title' => 'Last name',
 *       ],
 *       'telephone' => [
 *         'type' => 'string',
 *         'title' => 'Telephone',
 *         'minLength' => 10,
 *       ],
 *     ],
 *   ],
 *   '#uiSchema' => [
 *     'firstName' => [
 *       'ui:autofocus' => TRUE,
 *       'ui:emptyValue' => '',
 *       'ui:autocomplete' => 'family-name',
 *     ],
 *     'lastName' => [
 *       'ui:emptyValue' => '',
 *       'ui:autocomplete' => 'given-name',
 *     ],
 *     'age' => [
 *       'ui:widget' => 'updown',
 *       'ui:title' => 'Age of person',
 *       'ui:description' => '(earthian year)',
 *     ],
 *     'bio' => [
 *       'ui:widget' => 'textarea',
 *     ],
 *     'password' => [
 *       'ui:widget' => 'password',
 *       'ui:help' => 'Hint: Make it strong!',
 *     ],
 *     'date' => [
 *       'ui:widget' => 'alt-datetime',
 *     ],
 *     'telephone' => [
 *       'ui:options' => [
 *         'inputType' => 'tel',
 *       ],
 *     ],
 *   ],
 * ];
 * @endcode
 *
 * @FormElement("rjsf_editor")
 */
class RjsfEditor extends FormElement {

  public function getInfo() {
    return [
      '#schema' => [],
      '#uiSchema' => [],
      '#server_validation' => TRUE,
      '#client_validation' => TRUE,
      '#input' => TRUE,
      '#multiple' => FALSE,
      '#default_value' => new \stdClass(),
      '#attached' => [
        'library' => ['rjsf/editor'],
      ],
      '#theme_wrappers' => ['container'],
      '#process' => [
        [$this, 'processElement'],
      ],
      '#element_validate' => [
        [$this, 'validateInput'],
      ],
    ];
  }

  /**
   * Processes the rjsf_editor form element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   *
   * @throws \InvalidArgumentException
   *   Thrown when #available_countries is malformed.
   */
  public static function processElement(array &$element, FormStateInterface $form_state, array &$complete_form) {
    if (!isset($element['#schema'])) {
      throw new \InvalidArgumentException('The #schema property is required.');
    }
    else if (!is_array($element['#schema'])) {
      throw new \InvalidArgumentException('The #schema property must be an array.');
    }

    if (isset($element['#uiSchema']) && !is_array($element['#uiSchema'])) {
      throw new \InvalidArgumentException('The #uiSchema property must be an array.');
    }

    $wrapperId = Html::getUniqueId('rjsf-editor');

    $element['#tree'] = TRUE;
    $element['#attributes']['id'] = $wrapperId;
    $element['#attributes']['class'][] = 'rjsf-editor';

    // Add the hidden field that stores the values for this component.
    $element['value'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'class' => 'rjsf-editor-value',
        'data-rjsf-selector' => 'rjsf-editor-value',
        'style' => ['all: initial; background: white; display: inline-block; width: 100%;']
      ],
      '#default_value' => Json::encode($element['#default_value']),
    ];

    // Allow other modules to easily add additional definitions to the schema.
    $definitionEvent = new AddDefinitionsEvent($element['#schema'], $element['#uiSchema'], $element, $complete_form, $form_state);
    \Drupal::getContainer()->get('event_dispatcher')->dispatch($definitionEvent, RjsfEvents::ADD_DEFINITIONS);
    $existing_defs = $element['#schema']['definitions'] ?? [];
    $element['#schema']['definitions'] = array_merge($existing_defs, $definitionEvent->getDefinitions());

    // Add the target html element for RJSF and add the form schema and uiSchema
    // to the drupalSettings.
    $element['editor'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => 'rjsf-editor-target',
        'data-rjsf-selector' => 'rjsf-editor-target',
        'style' => ['all: initial; background: white; display: inline-block; width: 100%;']
      ],
      '#attached' => [
        'drupalSettings' => [
          'rjsf' => [
            $wrapperId => [
              'target' => $wrapperId,
              'schema' => $element['#schema'],
              'uiSchema' => $element['#uiSchema'],
              'clientValidate' => $element['#client_validation'],
            ],
          ],
        ],
      ],
    ];

    // Allow other modules to easily add libraries and drupalSettings to the
    // editor attachments.
    $attachmentEvent = new EditorAttachEvent($element['#schema'], $element['#uiSchema'], $element, $complete_form, $form_state);
    \Drupal::getContainer()->get('event_dispatcher')->dispatch($attachmentEvent, RjsfEvents::EDITOR_ATTACH);
    $element['editor']['#attached'] = BubbleableMetadata::mergeAttachments($element['editor']['#attached'], $attachmentEvent->getAttachments());

    // @TODO figure out how to do this conditionally for the rich text widget.

    return $element;
  }

  /**
   * Form element validation handler for #type 'rjsf_editor'.
   */
  public static function validateInput(&$element, FormStateInterface $form_state, &$complete_form) {
    if ($element['#server_validation'] === FALSE) {
      return;
    }

    $input_exists = FALSE;
    $input = NestedArray::getValue($form_state->getValues(), $element['#parents'], $input_exists);
    if ($input_exists) {
      $value = json_decode($input['value']);
      $validator = \Drupal::getContainer()->get('rjsf.schema.validator');
      $validationResult = $validator->validate($value, JSON::encode($element['#schema']));

      if ($validationResult->isValid()) {
        $form_state->setValueForElement($element, $input);
      }
      else {
        // @TODO improve error reporting by passing the detailed information via drupalSettings to extraErrors in RJSF.
        // @see https://opis.io/json-schema/2.x/php-validator.html
        // @see https://react-jsonschema-form.readthedocs.io/en/latest/usage/validation/#async-validation
        $title = !empty($element['#title']) ? $element['#title'] : 'form';

        $form_state->setError($element, t('The %field field has validation issues please correct the issues and resubmit the data.', ['%field' => $title]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    $value = new \stdClass();
    if (is_array($input) && isset($input['value'])) {
      $value = $input['value'];
    }

    return $value;
  }

}
