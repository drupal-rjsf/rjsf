import React from 'react';
import ReactDOM from 'react-dom';
import {withTheme} from '@rjsf/core';
import {Theme5 as MaterialUITheme} from '@rjsf/material-ui';
import {createTheme, ThemeProvider} from "@mui/material";

let widgetsLoaded = false;
let widgets = {};
let fields = {};
// @TODO make this a function that matches the php version.
let formats = {
  'entity_reference': '^[\\w_]+:\\d+',
  'rich_text':'[\\s\\S]*',
};

const theme = createTheme();

/**
 * Take the widget definitions and go through the process of adding the module
 * federation entry point and then loading the RJSF widget settings.
 */
async function loadWidgets() {
  let promises = [];

  let widgetResponse = await fetch('/jsonapi/rjsf_editor_widget/rjsf_editor_widget?filter[enable]=1');
  let definitions = await widgetResponse.json();
  for (const defs of definitions.data) {
    // @TODO choose between adding entry points via library or via js.
    loadJS(defs.attributes.url, document.head);
    promises.push(loadRemoteModules(defs.attributes.jsScope, defs.attributes.jsPlugin));
  }

  return Promise.all(promises);
}

/**
 * Add a javascript script tag to the page.
 *
 * @param url
 *   The url for the script to add to the page.
 * @param location
 *   The location to add the script within the page.
 */
var loadJS = function(url, location){
  var scriptTag = document.createElement('script');
  scriptTag.src = url;
  scriptTag.async = false;

  location.appendChild(scriptTag);
};

/**
 * Load a federation module and the exports to RJSF widgets.
 *
 * @param scope
 *   The scope of the federated module.
 * @param module
 *   The module to load.
 *
 * @returns {Promise<*>}
 *   The module.
 */
async function loadRemoteModules(scope, module) {
  // Initializes the shared scope. Fills it with known provided modules from this build and all remotes
  await __webpack_init_sharing__('default');
  const container = window[scope]; // or get the container somewhere else
  // Initialize the container, it may provide shared modules
  await container.init(__webpack_share_scopes__.default);
  const factory = await window[scope].get(module);
  const Module = factory();

  // Add the exports to the list of available widgets.
  widgets = {...widgets, ...Module.widgets};
  fields = {...fields, ...Module.fields};
  formats = { ...formats, ...Module.formats};

  return Module;
}

const Form = withTheme(MaterialUITheme);

/**
 * Search a page for the given selector and if found remove it.
 *
 * @param selector
 *   The css selection rule to remove.
 */
function removeCssStyle(selector) {
  for (let i = 0; i < document.styleSheets.length; i++) {
    let stylesheet = document.styleSheets[i];

    try {
      stylesheet.cssRules.length;
    }
    catch (e) {
      continue;
    }

    for (let r = 0; r < stylesheet.cssRules.length; r++) {
      let rule = stylesheet.cssRules[r].selectorText;
      if (rule === selector) {
        document.styleSheets[i].deleteRule(r);
      }
    }
  }
}

(function ($, Drupal) {
  'use strict';

  /**
   * Run the initEditors function on script load. This is required for proper
   * interaction with layout builder dialogs. Because the editor is attached to
   * the block form on the first load of a RJSF element the events for attach
   * and dialog have already been fired before the js finished loading. This
   * necessitates running an editor init when the script is loaded.
   */
  initEditors();

  /**
   * Attach editors on pages that behave well with Drupal behaviors.
   */
  Drupal.behaviors.rjsfEditor = {
    attach: function attach(context) {
      initEditors();
    },
  };

  /**
   * Initialize the RJSF editor by loading all the custom widgets and then
   * rendering each editor defined in the settings.
   */
  function initEditors() {
    for (const target in drupalSettings.rjsf) {
      $('#' + target).once('rjsfEditor').each(function () {
        if (!widgetsLoaded) {
          loadWidgets().then(response => {
            renderEditor(target, drupalSettings.rjsf[target]);
            widgetsLoaded = true;
          });
        }
        else {
          renderEditor(target, drupalSettings.rjsf[target]);
        }

        // The resets and styling provided by Drupal/Gin/Gin LB are aggressive
        // and hard to work around. Until MUI properly supports shadow doms
        // we are stuck either providing our own more specific rules or
        // removing conflicting ones. https://github.com/mui/material-ui/issues/17473
        const rules = [
          '#drupal-off-canvas.ui-dialog-content div',
          '.glb-form-wrapper label, .glb-form-composite label',
          '.ui-dialog fieldset:not(.fieldgroup)',
          'fieldset:not(.fieldgroup)',
          '.ui-dialog fieldset:not(.fieldgroup) > legend',
          'fieldset:not(.fieldgroup)>legend',
        ];

        rules.forEach((rule) => {
          removeCssStyle(rule);
        });
      });
    }

    // Override Drupal's ajax submit handling to force the submit button to respect RJSF clientside validation.
    // Largely copied from the clientside_validation module.
    if (typeof Drupal.Ajax !== 'undefined') {
      // Update Drupal.Ajax.prototype.beforeSend only once.
      if (typeof Drupal.Ajax.prototype.beforeSubmitRJSFOriginal === 'undefined') {
        Drupal.Ajax.prototype.beforeSubmitRJSFOriginal = Drupal.Ajax.prototype.beforeSubmit;
        Drupal.Ajax.prototype.beforeSubmit = function (form_values, element_settings, options) {
          if (typeof this.$form !== 'undefined' && this.$form.find('.rjsf-editor').length > 0) {
            const rjsfTarget = this.$form.find('.rjsf-editor').attr('id');
            const clientValidate = drupalSettings.rjsf[rjsfTarget].clientValidate;

            if (clientValidate && !checkRjsfFormValidity(this.$form.get(0))) {
              this.ajaxing = false;
              return false;
            }
          }

          return this.beforeSubmitRJSFOriginal.apply(this, arguments);
        };
      }
    }
}

  /**
   * Render the RJSF form in the passed target id.
   */
  function renderEditor(target, config) {
    let valueElement = $('#' + target + ' [data-rjsf-selector="rjsf-editor-value"]');
    let saveFormData = function (value) {
      valueElement.val(JSON.stringify(value));
      config.formData = value;
    };

    let values = {};
    if (valueElement.length !== 0) {
      values = JSON.parse(valueElement.val());
    }
    else if(config.formData !== undefined) {
      values = config.formData;
    }

    const schema = config.schema;
    const uiSchema = config.uiSchema;
    const clientValidate = config.clientValidate;
    const formData = values;

    const editorTarget = document.getElementById(target).querySelector(' [data-rjsf-selector="rjsf-editor-target"]');

    // @TODO Render this in a shadow root one supported by MUI https://github.com/mui/material-ui/issues/17473
    // @TODO figure out how to support omitExtraData and liveOmit with entity reference fields. Global definitions perhaps?
    // omitExtraData and liveOmit can not be enabled without defining the entity_reference schema for every entity reference field in every form which is annoying.
    ReactDOM.render((
      <ThemeProvider theme={theme}>
        <Form
        tagName="div"
        children={true}
        widgets={widgets}
        schema={schema}
        uiSchema={uiSchema}
        formData={formData}
        customFormats={formats}
        fields={fields}
        onChange={e => saveFormData(e.formData)}
        clientValidate={clientValidate}
        noHtml5Validate
      />
    </ThemeProvider>), editorTarget);
  }

  /**
   * Check if RJSF elements in a form are valid.
   *
   * @param form
   *   The form containing rjsf data to check for validity.
   * @returns boolean
   *   Returns TRUE if the form is valid and FALSE is the form has errors.
   */
  function checkRjsfFormValidity(form) {
    /**
     * This seems to be the cleanest way to force RJSF to validate forms when
     * it doesn't correctly capture the submit event like on layout builder
     * ajax forms.
     */
    return form.reportValidity();
  }

})(jQuery, Drupal);
