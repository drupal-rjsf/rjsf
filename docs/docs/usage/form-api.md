---
sidebar_position: 1
---

# Form API
 Using the `rjsf_editor` element type RJSF forms can be added to any Drupal form. Set the schema, uiSchema, and renderPreprocess definitions in the element settings and then the RJSF form will render insde a div element in the Drupal form. See [Schema Structure](./schema-structure.md) for available schema properties and what they do.

### Usage Example
```php
$form['rjsf_element'] = [
  '#type' => 'rjsf_editor',
  '#server_validation' => TRUE,
  '#client_validation' => TRUE,
  '#schema' => [
    'title' => 'A registration form',
    'description' => 'A simple form example.',
    'type' => 'object',
    'required' => ['firstName', 'lastName'],
    'properties' => [
      'firstName' => [
        'type' => 'string',
        'title' => 'First name',
        'default' => 'Chuck',
      ],
      'lastName' => [
        'type' => 'string',
        'title' => 'Last name',
      ],
      'telephone' => [
        'type' => 'string',
        'title' => 'Telephone',
        'minLength' => 10,
      ],
    ],
  ],
  '#uiSchema' => [
    'firstName' => [
      'ui:autofocus' => TRUE,
      'ui:emptyValue' => '',
      'ui:autocomplete' => 'family-name',
    ],
    'lastName' => [
      'ui:emptyValue' => '',
      'ui:autocomplete' => 'given-name',
    ],
    'age' => [
      'ui:widget' => 'updown',
      'ui:title' => 'Age of person',
      'ui:description' => '(earthian year)',
    ],
    'bio' => [
      'ui:widget' => 'textarea',
    ],
    'password' => [
      'ui:widget' => 'password',
      'ui:help' => 'Hint: Make it strong!',
    ],
    'date' => [
      'ui:widget' => 'alt-datetime',
    ],
    'telephone' => [
      'ui:options' => [
        'inputType' => 'tel',
      ],
    ],
  ],
];
```

## Accessing the data
When a form is submitted the RJSF data is available as json string under the `values` attribute of the form element. One way to access this is
```php
Json::decode($form_state->getValue(['rjsf_element','value']));
```
