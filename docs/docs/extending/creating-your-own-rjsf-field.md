# Creating your own RJSF field

Additional editor elements can be added to the editor by creating your own RJSF element and then adding it to the editor via [Module Federation](https://module-federation.github.io/). To see the pattern in use checkout the submodules under the `/modules` folder and for a more detailed example and instructions on implementing your own see [rjsf_example_widget](https://git.drupalcode.org/project/rjsf/-/tree/1.0.x/modules/rjsf_example_widget) and README.

To add a new element you'll roughly need to

- Determine if you are creating a RJSF widget or field. See the [RJSF docs here](https://react-jsonschema-form.readthedocs.io/en/latest/advanced-customization/custom-widgets-fields/) for more details on the differences
- Create a new Drupal module(or use an existing one)
- Decide on a namespace for the new element
- Determine if the element will need any new definitions, formats, or filters
- Determine if you're pulling data from Drupal and if so what APIs will be used to retrieve data?
- Implement the RJSF React element with support for the various properties available to fields/widgets(required, disabled, default values, etc)
- Document usage examples, new filters, new formats, new defintions in the module's README
  - Never trust the data submitted until it's been validated on the backend by the [validator service](../validation)

### IMPORTANT if the elements' data can't be validated purely using JSON Schema validation you need to implement a filter so the data can be validated on the backend. This is almost always required when dealing with references to Drupal entities or pulling data from an API.
