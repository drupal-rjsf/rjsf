# link

This preprocessor takes link data and returns the Drupal path. Depending on the original data this plugin runs the input through Drupal's `Url::fromtRoute`,`Url::fromUri`, or `$entity->toUrl()->toString()`. This means that Drupal's special `<nolink>`, `none>`, and `<front>` tags are supported.

## Variables
This plugin doesn't support any variables.

## Adding to your schema
This example uses a link field but any field that matches the expected data structure would also work.

```json
{
  "schema":{
    "properties":{
      "linkField":{
        "type":"object",
        "properties":{
          "url":{
            "type":"string"
          },
          "entity":{
            "type":"object"
          },
          "title":{
            "type":"string"
          }
        },
        "$filters":{
          "$func":"link",
          "$vars":{
            "target_type":"node",
            "handler":"default",
            "handler_settings":{
              "target_bundles":null,
              "sort":{
                "field":"_none",
                "direction":"ASC"
              }
            }
          }
        }
      }
    }
  },
  "uiSchema":{
    "linkField":{
      "ui:field":"link"
    }
  },
  "renderPreprocess":{
    "linkField":{
      "$plugins":{
        "$plugin":"link"
      }
    }
  }
}
```

## Expected data
Link data.
```php
[
  [
    'url' => '',
    'title' => '',
  ]
]
```

or entity data

```php
[
  [
    'entity' => [
      'uuid' => '',
      'id' => '',
      'type' => '',
      'bundle' => '',
    ],
    'title' => '',
  ]
]
```

## Output
```php
[
  'url' => '', // The url based on the data, this is run through Drupal's Url processing.
  'title' => '',
]
```
