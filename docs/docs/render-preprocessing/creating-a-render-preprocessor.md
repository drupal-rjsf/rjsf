# Creating a render preprocessor

Render preprocessors are defined as Drupal `RjsfRenderPreprocess` plugins in modules. To create your own preprocessor plugin:

1. Navigate to the custom module that will contain your plugin
2. Open or create `custom_module/src/Plugin/Rjsf/RenderPreprocess`
3. Create a new class `ExamplePlugin.php`
4. Extend `RenderPreprocessPluginBase`
5. Populate the annotation and `preprocess()` method

## Example
Additional examples can be found in the [RJSF module](https://git.drupalcode.org/project/rjsf/-/tree/1.0.x/src/Plugin/Rjsf/RenderPreprocess).

```php
<?php

namespace Drupal\custom_module\Plugin\Rjsf\RenderPreprocess;

/**
 * @RjsfRenderPreprocess(
 *  id = "example_preprocess",
 *  label = @Translation("Example preprocess"),
 * )
 */
class ExamplePlugin extends RenderPreprocessPluginBase {

  public function preprocess($value, array $vars = [], array $schema = [], array $uiSchema = []) {
   // Do preprocessing logic here
   $processed = '';

    return $processed;
  }

}
```

## Arguments
### value
This will contain the value of the property targeted in the `renderPreprocess` part of a schema. Currently preprocessors do not have access to the complete form data but that [may change in the future](https://www.drupal.org/project/rjsf/issues/3264343).

### vars
Any vars defined with the plugin definition in a `renderPreprocess`.

### schema
The full schema definition.

### uiSchema
The full uiSchema defintion.