---
sidebar_position: 2
---

# Installation

RJSF comes in two parts, the Drupal module and an accompanying npm library, both must be installed for the module to work.

1. Install the RJSF Drupal module
   1. Run `composer require drupal/rjsf`
2. Install the Drupal RJSF NPM library [@drupal-rjsf/editor](https://www.npmjs.com/package/@drupal-rjsf/editor)
   1. The easiest way to install the npm library that provides the editor and widgets is to use asset-packagist. The add assets packagist to your project see [here](https://www.drupal.org/docs/develop/using-composer/using-composer-to-install-drupal-and-manage-dependencies#third-party-libraries)
      1. After adding asset packagist run `composer require npm-asset/drupal-rjsf--editor`
   2. Alternatively the package can be downloaded from NPM and manually placed in the libraries folder. The expected path for the `rjsf_editor.min.js` file is `{web root}/libraries/drupal-rjsf--editor/dist/rjsf_editor.min.js`
3. Enable the RJSF module
   1. Run `drush en rjsf` or navigate to your sites extend page and enable RJSF
