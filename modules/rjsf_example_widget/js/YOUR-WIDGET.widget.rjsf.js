import React, { useEffect } from 'react';

// Build a React based editor widget here. See the other implementations provided in rjsf/modules for examples.
// @see https://react-jsonschema-form.readthedocs.io/en/latest/advanced-customization/custom-widgets-fields/

// export let name = 'YOUR_WIDGET_NAME';
// export let formats = {
//   'YOUR_CUSTOM_FORMAT': '^[\\w_]+:\\d+'
// };
// export {YOUR_WIDGET as widget};
