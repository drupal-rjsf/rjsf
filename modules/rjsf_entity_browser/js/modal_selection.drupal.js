/**
 * This file handles propagating the selected entities from en EntityBrowser modal display to the RJSF EntityBrowser widget.
 *
 * Note that this js runs within an iframe so we must go up to the parent for most calls.
 */
(function (drupalSettings) {
  'use strict';

  // Retrieve the values from entity browser.
  var input = parent.window.inputRef;
  var selected_entities = drupalSettings.entity_browser.rjsf_modal.entities;

  // Trigger React's event handler manually.
  var nativeInputValueSetter = parent.Object.getOwnPropertyDescriptor(parent.window.HTMLInputElement.prototype, "value").set;
  nativeInputValueSetter.call(input, JSON.stringify(selected_entities));

  var ev2 = new parent.Event('input', { bubbles: true});
  input.dispatchEvent(ev2);

  // Close the modal using the exposed react component method.
  parent.window.reactModal.handleClose();
}(drupalSettings));
