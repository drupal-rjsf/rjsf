import React, {useEffect, useRef, useState} from 'react';
import {
  AppBar,
  Box,
  Button,
  Dialog,
  IconButton,
  Toolbar,
  Typography
} from "@mui/material";
import {Close} from '@mui/icons-material';
import GridPreview from "./components/GridPreview";
import ListPreview from "./components/ListPreview";

/**
 * @TODO Rewrite this when I actually understand react.
 * @TODO Rewrite this as functional instead of a class.
 */

// Render the entity_browser iframe.
function IframePage({browser}) {
  // @TODO is a static uuid reference like `uuid=rjsf_modal` going to cause problems if multiple modals are on a page?
  return (
    <iframe
      src={'/entity-browser/rjsf_modal/' + browser + '?uuid=rjsf_modal'}
      className='entity-browser-modal-iframe'
      frameBorder='0'
      style={{padding: 0, position:'relative', zIndex:10002, display: 'block', width: '90vw', height: '100vh'}}
      name=''
      id={'entity_browser_iframe_' + browser }
    />
  );
}

function EntityBrowser ({
                          id,
                          placeholder,
                          formData,
                          required,
                          disabled,
                          autofocus,
                          label,
                          readonly,
                          onBlur,
                          onFocus,
                          onChange,
                          idSchema,
                          uiSchema,
                          schema,
                          registry,
                          rawErrors = [],
                        }) {
  const [open, setOpen] = useState(false);
  const [displayValue, setDisplayValue] = useState(formData === undefined ? [] : formData.filter(v => Object.keys(v).length !== 0));
  const inputRef = useRef(null);
  const uiOptions = uiSchema['ui:options'];

  /**
   * Take the stored values and populate the initial state with enriched data
   * from json api.
   */
  useEffect(() => {
    (async () => {
      const newValues = [];

      if (formData) {
        const values = {};

        // Sort and group the saved value by type and bundle.
        formData.forEach((entity) => {
          // Split the individual entity values in their respective variables
          const type = entity.type;
          const bundle = entity.bundle;

          // Ensure the storage object has the entity type set.
          if (!values.hasOwnProperty(type)) {
            values[type] = {};
          }

          // Ensure the storage object has the entity bundlge set.
          if (!values[type].hasOwnProperty(bundle)) {
            values[type][bundle] = [];
          }

          // Add the entity to the storage object.
          values[type][bundle].push(entity);
        });

        // Fetch all the referenced entities via jsonapi. Due to how jsonapi works this has to be done as one call per bundle type.
        for (const entityType in values) {
          for (const entityBundle in values[entityType]) {
            // Create the filter that limits the results to just the referenced uuids.
            let filter = "filter[id][condition][path]=id&filter[id][condition][operator]=IN";
            for (let i = 0; i < values[entityType][entityBundle].length; i++) {
              const pos = i+1;
              filter += "&filter[id][condition][value][" + pos + "]=" + values[entityType][entityBundle][i]['uuid'];
            }

            // @TODO this thumbnail logic makes a lot of assumptions, is there a way to make it more general?
            let hasThumbnail = false;
            if (entityType === 'media') {
              filter += "&include=thumbnail";
              hasThumbnail = true;
            }
            const thumbnailStyle = uiOptions.thumbnailStyle ? uiOptions.thumbnailStyle : 'thumbnail';

            // @TODO use Drupal route builder?
            const endpoint = '/jsonapi/' + entityType + '/' + entityBundle + '?' + filter;
            const response = await fetch(endpoint);
            const responseData = await response.json();

            // Add the referenced entity data in the same format returned by the jsonrpc autocomplete endpoint.
            responseData.data.forEach((entity) => {
              // @TODO supposedly not all entities have titles, what do we do then?
              let thumbnail = null;
              if (hasThumbnail) {
                const thumbnailId = entity.relationships.thumbnail.data.id;

                for (const include of responseData.included) {
                  if (include.id === thumbnailId) {
                    for (const styles of include.attributes.image_style_uri) {
                      if (styles.hasOwnProperty(thumbnailStyle)) {
                        thumbnail = styles[thumbnailStyle];
                        break;
                      }
                    }

                    if (thumbnail !== null) {
                      break;
                    }
                  }
                }
              }

              const label = entityType === 'media' ? entity.attributes.name : entity.attributes.title;

              newValues.push({
                uuid: entity.id,
                type: entityType,
                bundle: entityBundle,
                label: label,
                thumbnail: hasThumbnail ? thumbnail : null
              });
            });
          }
        }

        const orderedNewValues = formData.map( (value) => {
          for(const newVal of newValues) {
            if (value.uuid === newVal.uuid) {
              // Pull the internal id from the saved value so we don't have to figure out how to find it in jsonapi.
              newVal.id = value.id;
              return newVal;
            }
          }

          // Gracefully handle the value not matching to a new value.
          return value;
        });

        // Update the initial state of the component with the enriched values.
        setDisplayValue(orderedNewValues);
      }
    })();
  }, []);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleValueChange = (values) => {
    onChange(values);
    setDisplayValue(values);
  }

  const Preview = ({variant = "list"}) => {
    if (variant === 'grid') {
      return <GridPreview values={displayValue} handleValueChange={handleValueChange}/>
    }
    else {
      return <ListPreview values={displayValue} handleValueChange={handleValueChange}/>
    }
  }

  // onChange doesn't fire for hidden inputs so use onInput instead.
  const onInput = event => {
    onChange(JSON.parse(event.target.value));
    setDisplayValue(JSON.parse(event.target.value));
  };

  const getButtonLabel = () => {
    // @TODO make this logic more robust, use values from entity browser itself?
    if (Array.isArray(displayValue) && displayValue.length >= 1) {
      return uiSchema?.['ui:options']?.selectButton?.change ? uiSchema['ui:options'].selectButton.change : 'Change selection';
    }
    else {
      return uiSchema?.['ui:options']?.selectButton?.add ? uiSchema['ui:options'].selectButton.add : 'Add media';
    }
  }

  // @TODO reverse engineer the contents of this setting.
  // @TODO figure out how to support a minimum?
  window.drupalSettings.entity_browser = {'rjsf_modal': {
      cardinality: schema.maxItems ? schema.maxItems : 1
    }};

  window.reactModal = {'handleClose': handleClose};

  const TitleField = registry.fields.TitleField;
  const DescriptionField = registry.fields.DescriptionField;

  return (
    <Box>
      <TitleField id={idSchema.$id + "__title"} title={(uiSchema['ui:title'] || schema.title) + ((required || schema?.minItems !== undefined) ? '*' : '')} />
      <DescriptionField id={idSchema.$id + "__description"} description={uiSchema['ui:description'] || schema.description} />

      <input
        ref={inputRef => {window.inputRef = inputRef}}
        id={id}
        type="hidden"
        disabled={readonly || disabled}
        onInput={onInput}
        formNoValidate="formnovalidate"
      />

      <Preview variant={uiOptions.preview ? uiOptions.preview : 'list'}/>

      <Dialog
        maxWidth={false}
        open={open}
        onClose={handleClose}
      >
        <AppBar style={{position: 'relative'}}>
          <Toolbar>
            <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
              <Close />
            </IconButton>
            <Typography variant="h6" component="span">
              { uiSchema?.['ui:options']?.['dialogTitle'] ? uiSchema['ui:options']['dialogTitle'] : 'Select media' }
            </Typography>
          </Toolbar>
        </AppBar>
        <IframePage browser={uiOptions.browser}/>
      </Dialog>
      <Button variant="contained" onClick={handleOpen}>{getButtonLabel()}</Button>
    </Box>
  );
}

export let fields = {'entity_browser': EntityBrowser};
