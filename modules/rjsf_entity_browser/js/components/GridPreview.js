import {Box, Grid, IconButton, useTheme} from "@mui/material";
import {DeleteOutline} from "@mui/icons-material";
import {DragDropContext, Draggable, Droppable} from "react-beautiful-dnd";
import React from "react";
import {makeStyles} from "@mui/styles";

function PreviewItem({item, index, removeItem}) {
  const theme = useTheme();

  const useStyles = makeStyles({
    item: {
      position: "relative",
      minHeight: "100px",
      display: "flex",
      "& img": {
        minHeight: "inherit"
      }
    },
    label: {
      alignSelf: "flex-end"
    },
    remove: {
      background: theme.palette.common.white,
      position: "absolute",
      top: "2px",
      right: "2px",
      padding: "8px",
      "&:hover, &.Mui-focusVisible": {
        '& svg path': {
          color: theme.palette.error.contrastText,
          fill: theme.palette.error.contrastText,
        },
        background: theme.palette.error.dark
      }
    }
  });
  const classes = useStyles();

  const handleRemoveItem = () => {
    removeItem(index);
  }

  let label = "";
  // Entities don't have a defined "preview" value so here's a list of sensible attempts at a preview display.
  if (item.thumbnail) {
    label = (<img src={item.thumbnail}/>);
  }
  else if (item.label) {
    let text = '';
    if (item.label) {
      text = item.label;
    }
    else if (item.bundle) {
      text = item.bundle + " " + item.id;
    }
    else {
      text = item.type + " " + item.id;
    }

    label = (<div className={classes.label}>{text}</div>);
  }

  return (
    <div className={classes.item}>
      {label}
      <IconButton aria-label="remove" className={classes.remove} size="small" onClick={handleRemoveItem}>
        <DeleteOutline/>
      </IconButton>
    </div>
  )
}

export default function GridPreview ({values, handleValueChange, variant = 'preview'}) {
  const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
  };

  const onDragEnd = (result) => {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    const items = reorder(
      values,
      result.source.index,
      result.destination.index
    );

    handleValueChange(items);
  }

  const removeItem = (index) => {
    values.splice(index, 1);
    handleValueChange(values);
  }

  const getGridStyle = isDraggingOver => ({
    //background: isDraggingOver ? 'lightblue' : 'lightgrey',
  });

  const getItemStyle = (isDragging, draggableStyle) => ({
    // styles we need to apply on draggables
    ...draggableStyle,

    ...(isDragging && {
      background: "rgb(235,235,235)"
    })
  });

  return (
    <Box p={2}>
      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable droppableId="droppable" direction="horizontal">
          {(provided, snapshot) => (
            <Grid container ref={provided.innerRef} spacing={2} style={getGridStyle(snapshot.isDraggingOver)}>
              {values.map((item, index) => (
                <Draggable key={item.uuid} draggableId={item.uuid} index={index} isDragDisabled={values.length === 1}>
                  {(provided, snapshot) => (
                    <Grid item
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                    >
                      <PreviewItem item={item} removeItem={removeItem} index={index}/>
                    </Grid>
                  )}
                </Draggable>
              ))}
              {provided.placeholder}
            </Grid>
          )}
        </Droppable>
      </DragDropContext>
    </Box>
  );
}
