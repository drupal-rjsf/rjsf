import {
  Box,
  IconButton,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText, useTheme
} from "@mui/material";
import {DeleteOutline} from "@mui/icons-material";
import {DragDropContext, Draggable, Droppable} from "react-beautiful-dnd";
import React from "react";
import {makeStyles} from "@mui/styles";

function PreviewItem({item, index, thumbnail = false, removeItem}) {
  const theme = useTheme();

  const useStyles = makeStyles({
    item: {
      position: "relative",
    },
    remove: {
      background: theme.palette.common.white,
      "&:hover, &.Mui-focusVisible": {
        '& svg path': {
          color: theme.palette.error.contrastText,
          fill: theme.palette.error.contrastText,
        },
        background: theme.palette.error.dark
      }
    }
  });
  const classes = useStyles();

  const handleRemoveItem = () => {
    removeItem(index);
  }

  let label = '';

  if (item.label) {
    label = item.label;
  }
  else if (item.bundle) {
    label = item.bundle + " " + item.id;
  }
  else {
    label = item.type + " " + item.id;
  }

  return (
    <Box >
      <ListItemText primary={label}/>
      <ListItemSecondaryAction>
        <IconButton aria-label="remove" className={classes.remove} size="small" edge="end" onClick={handleRemoveItem}>
          <DeleteOutline/>
        </IconButton>
      </ListItemSecondaryAction>
    </Box>
  );
}

export default function ListPreview ({values, handleValueChange, variant = 'preview'}) {
  const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
  };

  const onDragEnd = (result) => {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    const items = reorder(
      values,
      result.source.index,
      result.destination.index
    );

    handleValueChange(items);
  }

  const removeItem = (index) => {
    values.splice(index, 1);
    handleValueChange(values);
  }

  const getGridStyle = isDraggingOver => ({
    //background: isDraggingOver ? 'lightblue' : 'lightgrey',
  });

  const getItemStyle = (isDragging, draggableStyle) => ({
    // styles we need to apply on draggables
    ...draggableStyle,

    ...(isDragging && {
      background: "rgb(235,235,235)"
    })
  });

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Droppable droppableId="droppable" >
        {(provided, snapshot) => (
          <List ref={provided.innerRef} style={getGridStyle(snapshot.isDraggingOver)} dense>
            {values.map((item, index) => (
              <Draggable key={item.uuid} draggableId={item.uuid} index={index} isDragDisabled={values.length === 1}>
                {(provided, snapshot) => (
                  <ListItem
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                  >
                    <PreviewItem item={item} removeItem={removeItem} index={index}/>
                  </ListItem>
                )}
              </Draggable>
            ))}
            {provided.placeholder}
          </List>
        )}
      </Droppable>
    </DragDropContext>
  );
}
