const path = require('path');
const { ModuleFederationPlugin } = require("webpack").container;

module.exports = {
  entry: {
    entityBrowserPlugin: ["./js/entityBrowser.widget.rjsf.js"]
  },
  output: {
    filename: "[name].min.js",
    path: path.resolve(__dirname, "../../dist/entity_browser"),
    library: 'entity_browser_plugin',
    publicPath: '/libraries/drupal-rjsf--editor/dist/entity_browser/',
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: [["@babel/preset-env", { modules: false }]]
          }
        }
      }
    ]
  },
  plugins: [
    new ModuleFederationPlugin({
      name: "entity_browser_plugin",
      library: { type: "var", name: "entity_browser_plugin" },
      filename: "entityBrowserEntry.js",
      exposes: {
        './EntityBrowserPlugin':'./js/entityBrowser.widget.rjsf.js',
      },
      shared: {
        "react": { singleton: true, eager: true },
        "react-dom": { singleton: true, eager: true },
        "@rjsf/core": {singleton: true, eager: true },
        "@rjsf/material-ui": {singleton: true, eager: true },
        "@mui/styles": {singleton: true, eager: true },
        "@mui/material": {singleton: true, eager: true },
        "@mui/icons-material": {singleton: true, eager: true },
        "@emotion/react": {singleton: true, eager: true },
        "@emotion/styled": {singleton: true, eager: true }
      },
    }),
  ]
};
