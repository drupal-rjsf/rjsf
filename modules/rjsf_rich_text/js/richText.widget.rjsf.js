import React, {useEffect} from 'react';
import {
  Alert,
  Box,
  Button,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  FormControl,
  FormHelperText,
  InputLabel,
  MenuItem,
  Select
} from "@mui/material";
import {makeStyles} from "@mui/styles";

/**
 * @TODO how to handle not having permission to edit the field?
 */

/**
 * Filter away XSS attack vectors when switching text formats.
 *
 * @see core/modules/editor/js/editor.es6.js:37
 */
const filterXssWhenSwitching = (field, format, originalFormatID, callback) => {
  // A text editor that already is XSS-safe needs no additional measures.
  if (format.editor.isXssSafe) {
    callback(field, format);
  }
  // Otherwise, ensure XSS safety: let the server XSS filter this value.
  else {
    jQuery.ajax({
      url: Drupal.url(`editor/filter_xss/${format.format}`),
      type: 'POST',
      data: {
        value: field.value,
        original_format_id: originalFormatID,
      },
      dataType: 'json',
      success(xssFilteredValue) {
        // If the server returns false, then no XSS filtering is needed.
        if (xssFilteredValue !== false) {
          field.value = xssFilteredValue;
        }
        callback(field, format);
      },
    });
  }
};

/**
 * Build the full list of wanted formats.
 */
const wantedFormats = (formData, schema) => {
  let wantedFormats = [];
  if (formData !== undefined && formData.hasOwnProperty('format') && formData.format !== undefined) {
    wantedFormats.push(formData.format);
  }
  if (schema.$filters.$vars.default_format !== undefined) {
    wantedFormats.push(schema.$filters.$vars.default_format);
  }
  if (schema.$filters.$vars.allowed_formats !== undefined) {
    wantedFormats = [
      ...wantedFormats,
      ...schema.$filters.$vars.allowed_formats
    ];
  }
  return wantedFormats.filter((item, pos) => wantedFormats.indexOf(item) === pos);
}

/**
 * Get all the attachments for wanted formats and load them onto the page using
 * Drupal.ajax.
 */
const loadAvailableFormatAttachments = async (formData, schema) => {
  // @TODO is there a way to make this prettier? Do we have to use Drupal.ajax? Inspiration was initially drawn from the quickedit module.
  const loadEditorsAjax = Drupal.ajax({
    url: Drupal.url('rjsf_rich_text/attachments'),
    submit: {'formats': wantedFormats(formData, schema)},
  });
  // Implement a scoped insert AJAX command: calls the callback after all AJAX
  // command functions have been executed (hence the deferred calling).
  const realInsert = Drupal.AjaxCommands.prototype.insert;
  loadEditorsAjax.commands.insert = function(ajax, response, status) {
    realInsert(ajax, response, status);
  };
  // Trigger the AJAX request, which will return AJAX commands to
  // insert any missing attachments.
  return loadEditorsAjax.execute().promise();
};

/**
 * Wait for an editor to fully load on to the page.
 */
const waitForEditor = (wantedEditor, callback) => {
  if (Drupal.editors[wantedEditor] !== undefined) {
    callback();
  } else {
    setTimeout(() => {
      waitForEditor(wantedEditor, callback)
    }, 50);
  }
}

/**
 * Use a JSON:RPC method to retrieve the formats actually available for this
 * user.
 */
const getAvailableFormats = async (formData, schema) => {
    const formatsData = {
      "jsonrpc": "2.0",
      "method": "rich_text_formats",
      "id": "rich_text_formats",
      "params": { }
    };

    // @TODO use Drupal route builder?
    const endpoint = '/jsonrpc?query=' + encodeURIComponent(JSON.stringify(formatsData));

    const response = await fetch(endpoint);
    const data = await response.json();

    let formats = data.result.availableFormats;

    if (schema.$filters.$vars.allowed_formats !== undefined) {
      formats = data.result.availableFormats.flatMap(value => schema.$filters.$vars.allowed_formats.includes(value.id) ? value : []);
    }

    return {
      availableFormats: formats,
      fallbackFormat: data.result.fallbackFormat,
      userDefaultFormat: data.result.userDefaultFormat
    };
};

function RichTextWidget ({
    schema,
    uiSchema,
    idSchema,
    formData,
    errorSchema,
    registry,
    formContext,
    onChange
  }) {

  let [availableFormats, setAvailableFormats] = React.useState([{id: ''}]);
  let [userDefaultFormat, setUserDefaultFormat] = React.useState({});
  let [fallbackFormat, setFallbackFormat] = React.useState({});
  let textareaRef = React.createRef();
  let containerRef = React.createRef();
  let [dataValue, setDataValue] = React.useState({});
  let [showTextChangeModal, setShowTextChangeModal] = React.useState(false);
  let [newFormat, setNewFormat] = React.useState('');
  let [doneLoadingFormats, setDoneLoadingFormats] = React.useState(false);

  /**
   * Load all the attachments, available formats, and initialize state.
   */
  useEffect(() => {
    Promise.all([
      loadAvailableFormatAttachments(formData, schema),
      getAvailableFormats(formData, schema)
    ]).then((results) => {
      setAvailableFormats(results[1].availableFormats);
      setUserDefaultFormat(results[1].userDefaultFormat);
      setFallbackFormat(results[1].fallbackFormat);

      let inputs = {
        value: '',
        format: ''
      };

      if (formData) {
        if (formData.hasOwnProperty('value')) {
          inputs.value = formData.value;
        }

        // The format precedence should be:
        // 1. The saved or current format from the form.
        // 2. The configured default format from the schema IF it is available.
        // 3. The default format for the user IF it is available.
        // 4. The highest weighted format from the list of available formats.
        if (formData.hasOwnProperty('format') && formData.format !== '' && formData.format !== undefined) {
          inputs.format = formData.format;
        }
        else {
          let fieldDefaultFormat = schema.$filters?.$vars?.default_format;
          let hasFieldDefault = results[1].availableFormats.find(value => value.id === fieldDefaultFormat);
          let hasUserDefault = results[1].availableFormats.find(value => value.id === userDefaultFormat);

          // Pull the configured default format from the schema.
          if (fieldDefaultFormat !== undefined && hasFieldDefault) {
            inputs.format = fieldDefaultFormat;
          }
          else if (hasUserDefault) {
            inputs.format = userDefaultFormat;
          } else if (availableFormats.length !== 0) {
            inputs.format = availableFormats[0].id;
          }
          else {
            inputs.format = '';
          }
        }
      }
      else {
        inputs.format = schema.$filters?.$vars?.default_format;
      }

      setDataValue(inputs);
      onChange(inputs);
      setDoneLoadingFormats(true);
    });
  }, []);

  /**
   * Attach the Drupal behaviors.
   */
  useEffect(() => {
    if (doneLoadingFormats) {
      // Wait for the default editor to load on to the page before initializing
      // behaviors.
      waitForEditor(drupalSettings.editor.formats[dataValue.format].editor, () => {
        var container = jQuery(containerRef.current);
        Drupal.attachBehaviors(container.get(0));
      });
    }
  }, [doneLoadingFormats]);

  /**
   * Run the format change process only if there is a new format and the
   * modal is closed.
   */
  useEffect(() => {
    if (showTextChangeModal === false && newFormat !== '') {
      doTextFormatChange();
    }
  }, [showTextChangeModal, newFormat]);

  /**
   * Handle the input event for the textarea.
   */
  const _onInput = (event) => {
    // @TODO figure out sensible default value.
    setTextValue(event.target.value);
  };

  /**
   * Set the format of the editor.
   */
  const setFormat = (format) => {
    dataValue.format = format;
    setDataValue(dataValue);
    onChange(dataValue);
  };

  /**
   * Set the value of the editor.
   */
  const setTextValue = (textValue) => {
    dataValue.value = textValue;
    setDataValue(dataValue);
    onChange(dataValue);
  };

  /**
   * Check if the confirmation modal should be shown before changing the text
   * format.
   * @see core/modules/editor/js/editor.es6.js:111
   */
  const checkTextFormatChange = (format) => {
    // Move the current editor value into the textarea element.
    if (drupalSettings.editor.formats[dataValue.format]) {
      Drupal.editorDetach(textareaRef.current, drupalSettings.editor.formats[dataValue.format], 'serialize');
    }
    const activeFormatID = dataValue.format;
    const newFormatID = format;

    // Prevent double-attaching if the change event is triggered manually.
    if (newFormatID === activeFormatID) {
      return;
    }

    // When changing to a text format that has a text editor associated
    // with it that supports content filtering, then first ask for
    // confirmation, because switching text formats might cause certain
    // markup to be stripped away.
    const supportContentFiltering =
      drupalSettings.editor.formats[newFormatID] &&
      drupalSettings.editor.formats[newFormatID].editorSupportsContentFiltering;

    // If there is no content yet, it's always safe to change the text format.
    const hasContent = textareaRef.current.value !== '';
    if (hasContent && supportContentFiltering) {
      setShowTextChangeModal(true);
      setNewFormat(format);
    } else {
      setNewFormat(format);
    }
  };

  /**
   * Change the format of the editor and make sure XSS cleaning is handled.
   * @see core/modules/editor/js/editor.es6.js:73
   */
  const doTextFormatChange = () => {
    const newFormatID = newFormat;
    const previousFormatID = dataValue.format;
    const field = textareaRef.current;

    if (drupalSettings.editor.formats[previousFormatID]) {
      Drupal.editorDetach(
        field,
        drupalSettings.editor.formats[previousFormatID],
      );
    }
    // When no text editor is currently active, stop tracking changes.
    else {
      // @TODO what does this do? I think this is handled okay by just hiding the textarea?
      jQuery(field).off('.editor');
    }

    // Attach the new text editor (if any).
    if (drupalSettings.editor.formats[newFormatID]) {
      const format = drupalSettings.editor.formats[newFormatID];
      filterXssWhenSwitching(
        field,
        format,
        previousFormatID,
        Drupal.editorAttach,
      );
    }

    // Update the value stored in state with the newly applied format.
    setFormat(newFormatID);

    // Reset the state values related to changing the format.
    setNewFormat('');
    setShowTextChangeModal(false);
  };

  /**
   * Handle the 'Cancel' option in the change format confirmation modal.
   */
  const closeTextChangeModal = () => {
    setShowTextChangeModal(false);
    setNewFormat('');
  };

  /**
   * Take the available formats and turn them into components for the select
   * element.
   */
  const formatMenuItems = () => {
    let formatOptions = [];
    availableFormats.forEach((opt) => {
      formatOptions.push(<MenuItem value={opt.id}>{opt.label}</MenuItem> )
    });

    return formatOptions;
  }

  const getTextFormatDisplayValue = () => {
    if (newFormat && showTextChangeModal) {
      return newFormat;
    }
    else if(dataValue.format !== undefined) {
      return dataValue.format;
    }
    else {
      return '';
    }
  }

  // @TODO this is showing the current value instead of the new value...
  // @TODO find the label value of the format.
  let modalWarning = Drupal.t(
    'Changing the text format to %text_format will permanently remove content that is not allowed in that text format.<br><br>Save your changes before switching the text format to avoid losing data.',
    {
      '%text_format': newFormat === undefined ? '' : newFormat,
    },
  );

  const useStyles = makeStyles(() => ({
    textareaLabel: {
      position: "relative",
      transform: "unset",
      marginBottom: "8px",
    },
    formatLabel: {
      minWidth: 120,
    }
  }));
  const classes = useStyles();

  const hideLoadingIcon = () => {
    // @TODO figure out how to show the loading icon when text format is changed.
    return doneLoadingFormats;
  };

  const hideInsufficientPermissionAlert = () => {
    if (!doneLoadingFormats) {
      return true;
    }

    if (availableFormats.length === 0) {
      return false;
    }

    if (dataValue.format === '') {
      return true;
    }

    if (availableFormats.filter(value => value.id === dataValue.format).length !== 0) {
      return true;
    }

    return false;
  }

  return (
    <Box ref={containerRef}>
      <Box>
        <FormControl fullWidth={true}>
          <InputLabel className={classes.textareaLabel} htmlFor={idSchema["$id"] + "-value"}>{schema.title}</InputLabel>
          <FormHelperText id={idSchema["$id"] + "-description"}>{schema.description}</FormHelperText>
          <Box hidden={hideLoadingIcon()} >
            <CircularProgress />
          </Box>
          <Box hidden={hideInsufficientPermissionAlert()} >
            <Alert severity="error">
              {Drupal.t('Error: You do not have sufficient permissions to edit this field.')}
            </Alert>
          </Box>
          <textarea
            hidden={true}
            id={idSchema["$id"] + "-value"}
            ref={textareaRef}
            onInput={_onInput}
            defaultValue={dataValue.value}
            data-editor-value-original={dataValue.value}
            is-rjsf-editor="true"
            data-editor-value-is-changed={false}
            aria-describedby={idSchema["$id"] + "-description"}
          />
        </FormControl>
        {/*Prevent poorly scoped Drupal behaviors on the page from attempting to
        attach before we are ready */}
        { doneLoadingFormats &&
          <Box marginY={2} hidden={!doneLoadingFormats || !hideInsufficientPermissionAlert()}>
            <FormControl>
              <InputLabel className={classes.formatLabel} htmlFor={idSchema["$id"] + "-format"}>
                {Drupal.t("Text format")}
              </InputLabel>
              <Select
                id={idSchema["$id"] + "-format"}
                value={getTextFormatDisplayValue()}
                onChange={(event) => {checkTextFormatChange(event.target.value)}}
                is-rjsf-edtior="true"
                inputProps={{"data-editor-for":idSchema["$id"] + "-value"}}
              >
                {formatMenuItems()}
              </Select>
            </FormControl>
          </Box>
        }
      </Box>
      <Dialog open={showTextChangeModal}>
        <DialogTitle>
          {Drupal.t("Change text format?")}
        </DialogTitle>
        <DialogContent>
          <DialogContentText dangerouslySetInnerHTML={{__html:modalWarning}} />
        </DialogContent>
        <DialogActions>
          <Button onClick={closeTextChangeModal} color="primary">
            {Drupal.t("Cancel")}
          </Button>
          <Button onClick={doTextFormatChange} variant="contained" color="primary">
            {Drupal.t("Continue")}
          </Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
};

export let formats = {};
export let fields = {'rich_text': RichTextWidget}
