/**
 * This file handles triggering event propagation between Drupal's WYSIWYG editors and the RJSF RichText widget.
 */
(function ($, Drupal) {
  'use strict';

  // Store the original editorDetach function for later use.
  var parentEditorDetach = Drupal.editorDetach;

  // Override the default editorDetach so we can call the required event propagation for react.
  Drupal.editorDetach = function (field, format, trigger) {
    // Run the default editorDetach implementation.
    parentEditorDetach(field, format, trigger);

    // Use the same check from the parent and an additional check that the editor has been rendered by rjsf.
    if (format.editor && field.hasAttribute('is-rjsf-editor')) {
      // Trigger React's event handler manually.
      var nativeInputValueSetter = Object.getOwnPropertyDescriptor(window.HTMLTextAreaElement.prototype, "value").set;
      nativeInputValueSetter.call(field, field.value);

      var ev = new Event('input', { bubbles: true});
      field.dispatchEvent(ev);
    }
  };
})(jQuery, Drupal);
