import React, {useState, useEffect} from "react";
import Button from "@mui/material/Button";
import ColorGroup from "./ColorGroup";
import Box from "@mui/material/Box";
import Stack from "@mui/material/Stack";

const Picker = ({
                  current,
                  setCurrent,
                  getColorHex,
                  getColorLabel,
                  schema,
                  getColorStyle,
                  handleClick,
                  required,
                  options
                }) => {
  const colorProps = options.colorProps;
  const [groupList, setGroupList] = useState([]);
  const [defaultGroupColors, setDefaultGroupColors] = useState([]);

  useEffect(() => {
    //get a list of colors that have no group info associated
    let colors = [];
    let displayGroups = options.displayGroups;

    for (let opt of schema.enum) {
      if (opt === null) {
        continue;
      }

      if (!displayGroups || colorProps[opt]?.group === undefined) {
        colors.push(opt);
      }
    }

    if (colors) {
      setDefaultGroupColors(colors);
    }

    //get a list of groups
    let groups = [];
    if (displayGroups && colorProps !== undefined) {
      for (let key in colorProps) {
        if (colorProps[key].group !== undefined) {
          for (let group in colorProps[key].group) {
            if (!groups.includes(colorProps[key].group[group])) {
              groups.push(colorProps[key].group[group]);
            }
          }
        }
      }
      setGroupList(groups);
    }
  }, [colorProps, options.displayGroups, schema.enum]);

  const clearSelection = (event) => {
    setCurrent(null);
    handleClick("bottom-start")(event);
  };

  let render = [];
  if (groupList !== []) {
    let colorList = [];
    for (let index in groupList) {
      colorList[index] = [];
      for (let key in colorProps) {
        let value = colorProps[key];
        if (value.group !== undefined) {
          if (Object.values(value.group).indexOf(groupList[index]) > -1) {
            colorList[index].push(key);
          }
        }
      }

      render.push(
        <ColorGroup
          groupName={groupList[index]}
          colorList={colorList[index]}
          current={current}
          required={required}
          setCurrent={setCurrent}
          getColorHex={getColorHex}
          getColorLabel={getColorLabel}
          getColorStyle={getColorStyle}
          key={index}
        />
      );
    }

    return (
      <Box sx={{padding: 2}}>
        <ColorGroup
          groupName=""
          colorList={defaultGroupColors}
          current={current}
          setCurrent={setCurrent}
          getColorHex={getColorHex}
          getColorLabel={getColorLabel}
          getColorStyle={getColorStyle}
        />

        <div>{render}</div>
        <Stack spacing={2} direction="row">
          <Button variant="contained" onClick={handleClick("bottom-start")}>
            Select
          </Button>
          {required ? null :
            (<Button variant="outlined" onClick={clearSelection}>
              Clear
            </Button>)}
        </Stack>
      </Box>
    );
  }
};

export default Picker;
