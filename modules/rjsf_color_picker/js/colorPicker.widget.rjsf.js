import React, {useState, useEffect, useRef, useCallback} from "react";
import reactCSS from "reactcss";
import Popper from "@mui/material/Popper";
import Box from "@mui/material/Box";
import Fade from "@mui/material/Fade";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import CloseOutlinedIcon from '@mui/icons-material/CloseOutlined';
import Picker from "./components/Picker";
import {ClickAwayListener, FormControl, InputLabel} from "@mui/material";

const convertStylesStringToObject = (stringStyles) =>
  typeof stringStyles === "string"
    ? stringStyles.split(";").reduce((acc, style) => {
        const colonPosition = style.indexOf(":");

        if (colonPosition === -1) {
          return acc;
        }

        const camelCaseProperty = style
            .substring(0, colonPosition)
            .trim()
            .replace(/^-ms-/, "ms-")
            .replace(/-./g, (c) => c.substring(1).toUpperCase()),
          value = style.substring(colonPosition + 1).trim();

        return value ? { ...acc, [camelCaseProperty]: value } : acc;
      }, {})
    : {};

const ColorPicker = ({
    id,
    schema,
    uiSchema,
    value,
    placeholder,
    required,
    disabled,
    readonly,
    autofocus,
    label,
    multiple,
    onChange,
    onKeyChange,
    onBlur,
    onFocus,
    options,
    formContext,
    rawErrors,
    registry,
  }) => {
  const [displayColorPicker, setDisplayColorPicker] = useState(false);
  const [current, setCurrent] = useState(value);
  const [currentStyle, setCurrentStyle] = useState(null);

  const anchorEl = useRef(null);
  const [open, setOpen] = useState(false);
  const [placement, setPlacement] = useState();

  const handleClick = (newPlacement) => (event) => {
    if (disabled || readonly) {
      return;
    }
    setOpen((prev) => placement !== newPlacement || !prev);
    setPlacement(newPlacement);
    setDisplayColorPicker(!displayColorPicker);
  };

  const handleClose = (event) => {
    setOpen(false);
  };

  const getColorHex = useCallback((colorKey) => {
    if (options?.colorProps[colorKey]?.style !== undefined) {
      return null;
    } else if (options?.colorProps[colorKey]?.hex !== undefined) {
      return options?.colorProps[colorKey].hex;
    } else {
      return colorKey;
    }
  },[options?.colorProps]);

  const getColorLabel = useCallback((colorKey) => {
    const index = schema?.enum.indexOf(colorKey);
    if (schema?.enumNames) {
      return schema.enumNames[index];
    } else {
      return colorKey;
    }
  },[schema?.enum, schema?.enumNames]);

  const getColorStyle = (colorKey) => {
    if (options?.colorProps[colorKey]?.style !== undefined) {
      return convertStylesStringToObject(options.colorProps[colorKey].style);
    } else {
      return null;
    }
  };

  useEffect(() => {
    if (getColorStyle(current) !== null) {
      setCurrentStyle(getColorStyle(current));
    } else if (getColorHex(current) !== null) {
      setCurrentStyle({ backgroundColor: getColorHex(current) });
    } else {
      setCurrentStyle(null);
    }

    if (current !== value) {
      onChange(current);
    }
  }, [current]);

  const styles = reactCSS({
    default: {
      label: {
        position: "unset",
        transform: "unset",
      },
      description: {
        marginLeft: "0",
      },
      colorBox: {
        width: "2.1875rem",
        height: "2.1875rem",
        borderRadius: "0.25rem",
        cursor: "pointer",
        border: "1px solid gray",
        position: "relative",
        overflow: "hidden",
      },
      noSelection: {
        fontSize: "2.8125rem",
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)",
      },
      color: {
        ...currentStyle,
        position: "relative",
        width: "100%",
        height: "100%",
        borderRadius: "0.188rem",
      },
      colorGrid: {
        position: "absolute",
        height: "100%",
        width: "100%",
        background:
          'url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAADFJREFUOE9jZGBgEGHAD97gk2YcNYBhmIQBgWSAP52AwoAQwJvQRg1gACckQoC2gQgAIF8IscwEtKYAAAAASUVORK5CYII=") left center',
      },
    },
  });

  // Is there a more elegant way to handle either of these states?
  let moreTitleText = '';
  if (disabled) {
    moreTitleText = '(disabled)'
  }
  if (readonly) {
    moreTitleText = '(read only)'
  }

  return (
    <FormControl>
      <InputLabel id={id + "__title"} required={required} style={styles.label}>{(options.title || schema.title) + moreTitleText}</InputLabel>
      <Box position={"relative"} ref={anchorEl}>
        <Popper
          style={{ zIndex: "1500" }}
          open={open}
          anchorEl={anchorEl.current}
          placement={placement}
          transition
          disablePortal
        >
          {({ TransitionProps }) => (
            <Fade {...TransitionProps} timeout={350}>
              <Paper sx={{ maxWidth: 300 }}>
                <ClickAwayListener onClickAway={handleClose}>
                  <div>
                  <Picker
                    current={current}
                    setCurrent={setCurrent}
                    getColorHex={getColorHex}
                    getColorLabel={getColorLabel}
                    getColorStyle={getColorStyle}
                    schema={schema}
                    options={options}
                    handleClick={handleClick}
                    required={required}
                    />
                  </div>
                </ClickAwayListener>
              </Paper>
            </Fade>
          )}
        </Popper>
        <div style={styles.colorBox}>
          <div style={styles.colorGrid}>
            {current == null &&
              <CloseOutlinedIcon style={styles.noSelection}/>
            }
          </div>
          <div onClick={handleClick("bottom-start")} style={styles.color}></div>
        </div>
        <Typography variant={"subtitle2"}>
          {getColorLabel(current) ? getColorLabel(current) : "Select a color"}
        </Typography>
      </Box>
    </FormControl>
  );
};

ColorPicker.defaultProps = {
  options: {
    displayGroups: true,
  }
};

export let formats = {};
export let widgets = { color_picker: ColorPicker };
export let fields = {};
