const path = require('path');
const { ModuleFederationPlugin } = require("webpack").container;

module.exports = {
  entry: {
    linkPlugin: ["./js/link.widget.rjsf.js"]
  },
  output: {
    filename: "[name].min.js",
    path: path.resolve(__dirname, "../../dist/link"),
    // Plugin namespace
    library: 'link_plugin',
    // The absolute publicly accessible path to the directory containing the built entry point.
    publicPath: '/libraries/drupal-rjsf--editor/dist/link/',
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: [["@babel/preset-env", { modules: false }]]
          }
        }
      }
    ]
  },
  plugins: [
    new ModuleFederationPlugin({
      name: "link_plugin",
      library: { type: "var", name: "link_plugin" },
      filename: "linkEntry.js",
      exposes: {
        './LinkPlugin':'./js/link.widget.rjsf.js',
      },
      shared: {
        "react": { singleton: true, eager: true },
        "react-dom": { singleton: true, eager: true },
        "@rjsf/core": {singleton: true, eager: true },
        "@rjsf/material-ui": {singleton: true, eager: true },
        "@mui/styles": {singleton: true, eager: true },
        "@mui/material": {singleton: true, eager: true },
        "@mui/icons-material": {singleton: true, eager: true },
        "@emotion/react": {singleton: true, eager: true },
        "@emotion/styled": {singleton: true, eager: true }
      },
    }),
  ]
};
