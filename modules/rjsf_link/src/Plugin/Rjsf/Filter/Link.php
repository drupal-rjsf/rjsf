<?php

namespace Drupal\rjsf_link\Plugin\Rjsf\Filter;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\rjsf\Plugin\FilterPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin to validate link fields.
 *
 * @RjsfFilter(
 *  id = "link",
 *  label = @Translation("Link"),
 *  type = {"object"},
 * )
 */
class Link extends FilterPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The selection plugin manager.
   *
   * @var \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface
   */
  protected SelectionPluginManagerInterface $selectionManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Link constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SelectionPluginManagerInterface $selection_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->selectionManager = $selection_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.entity_reference_selection'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Validate if the link value is valid.
   *
   * @param object $value
   *   The value to validate.
   * @param array $args
   *   The settings passed in the field schema.
   *
   * @return bool
   *   If the value is valid or not.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function validate($value, array $args = []): bool {
    $arrEntity = (array)($value->entity);
    if ($value === NULL || (empty($arrEntity) && empty($value->url))) {
      return TRUE;
    }
    else {
      if ($value->url) {
        $uri = trim($value->url);
        if (!$uri) {
          return FALSE;
        }
        elseif (in_array($uri,['<nolink>', '<none>', '<button>', '<front>'])) {
          // @TODO are all these actually supported?
          return TRUE;
        }
        elseif (in_array($uri[0], ['/', '?', '#'], TRUE) && !preg_match('/\s/', $uri)) {
          return TRUE;
        }
        else {
          if (UrlHelper::isValid($uri, TRUE)) {
            return TRUE;
          }
          else {
            return FALSE;
          }
        }
      }
      elseif ($arrEntity) {
        $handlerSettings = json_decode(json_encode($args['handler_settings']), TRUE) ?? [];
        $handlerSettings = $handlerSettings + [
          'handler' => $args['handler'] ?? 'default',
        ];
        return $this->checkEntity($value->entity, $handlerSettings);
      }
    }
    return TRUE;
  }

  /**
   * Validate if the values are valid entity references.
   *
   * @param object $value
   *   The entity data to validate.
   * @param array $handlerSettings
   *   The entity selector settings.
   *
   * @return bool
   *   If the entity information is valid or not.
   *
   * @TODO consolidate this with the same logic in EntityAutocomplete.
   * @see \Drupal\rjsf_entity_autocomplete\Plugin\Rjsf\Filter\EntityAutocomplete::validate()
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function checkEntity(object $value, array $handlerSettings) {
    $target_uuids[$value->type] ?? $target_uuids[$value->type] = [];
    $target_uuids[$value->type][] = $value->uuid;

    foreach ($target_uuids as $target_type => $uuids) {
      $entities = $this->entityTypeManager->getStorage($target_type)->loadByProperties(
        ['uuid' => $uuids]
      );

      $target_ids = [];
      foreach ($entities as $entity) {
        $target_ids[] = $entity->id();
      }

      // Create a default selection handler for each entity type submitted and
      // check if it is valid to reference it.
      $handler = $this->selectionManager->getInstance(
        $handlerSettings + [
          'target_type' => $target_type,
        ]
      );

      $valid_target_ids = $handler->validateReferenceableEntities($target_ids);
      if ($invalid_target_ids = array_diff($target_ids, $valid_target_ids)) {
        return FALSE;
        // @TODO improve error responses similar to how core does them in docroot/core/lib/Drupal/Core/Entity/Plugin/Validation/Constraint/ValidReferenceConstraintValidator.php:134
      }
    }

    return TRUE;
  }

}
